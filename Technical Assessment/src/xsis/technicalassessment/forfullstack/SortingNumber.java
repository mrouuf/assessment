package xsis.technicalassessment.forfullstack;

public class SortingNumber {

    public static void main(String[] args) {

        int[] angka = {-5, 12, 10, 12345, -25};

        printMin(angka);
        printMax(angka);
    }

    private static void printMax(int[] angka) {
        for (int i = 0; i < angka.length - 1; i++) {
            for (int j = i + 1; j < angka.length; j++) {
                if ( angka[i] < angka[j]){
                    int s = angka[j];
                    angka[j] = angka[i];
                    angka[i] = s;
                }
            }
        }

        for (int i = 0; i < angka.length-(angka.length-1); i++) {
            System.out.println("angka terbesar : "+angka[i]);
        }
    }

    private static void printMin(int[] angka) {
        for (int i = 0; i < angka.length - 1; i++) {
            for (int j = i + 1; j < angka.length; j++) {
                if ( angka[i] > angka[j]){
                    int s = angka[i];
                    angka[i] = angka[j];
                    angka[j] = s;
                }
            }
        }

        for (int i = 0; i < angka.length-(angka.length-1); i++) {
            System.out.println("angka terkecil : "+angka[i]);
        }
    }
}

/*made by rouuf*/
